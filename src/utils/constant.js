/*
 * @Author: Do not edit
 * @Date: 2023-05-16 10:52:03
 * @LastEditTime: 2023-05-16 16:05:24
 * @LastEditors: 元辰
 */
export const BROWSER = [
	{ name: 'Chrome', info: 'Google Chrome or Chromium' }, //0
	{ name: 'Firefox', info: 'Mozilla Firefox' }, //1
	{ name: 'Edg', info: 'Microsoft Edge' }, //2
	{ name: 'Opera', info: 'Opera' }, //3
	{ name: 'Trident', info: 'Microsoft Internet Explorer' }, //4
	{ name: 'Safari', info: 'Apple Safari' }, //5
	{ name: 'KHTML', info: 'Konqueror' }, //6
];

export const DEVICE = [
	{ name: 'Windows NT 10.0', info: 'Windows 10' }, //0
	{ name: 'Windows NT 6.2', info: 'Windows 8' }, //1
	{ name: 'Windows NT 6.1', info: 'Windows 7' }, //2
	{ name: 'Windows NT 6.0', info: 'Windows Vista' }, //3
	{ name: 'Windows NT 5.1', info: 'Windows XP' }, //4
	{ name: 'Windows NT 5.0', info: 'Windows 2000' }, //5
	{ name: 'Mac', info: 'Mac/iOS' }, //6
	{ name: 'X11', info: 'UNIX' }, //7
	{ name: 'Linux', info: 'Linux' }, //8
	{ name: 'Android', info: 'Android' }, //9
	{ name: 'ios', info: 'IOS' }, //10
];
