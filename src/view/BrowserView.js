/*
 * @Author: 元辰
 * @Date: 2023-05-15 17:11:02
 * @LastEditTime: 2023-05-16 14:54:28
 * @LastEditors: 元辰
 * @PS: 获取浏览器信息
 */
import { BROWSER } from '../utils/constant.js';

export const getBrowserView = () => {
	const { userAgent, language, cookieEnabled, buildID, mediaSession, pdfViewerEnabled, permissions, presentation, serviceWorker, storage, userActivation, wakeLock, webdriver, windowControlsOverlay } =
		navigator;

	let browserData = {
		browser: 'unknown',
		isBrowser: true,
		language,
		cookieEnabled,
		buildID,
		mediaSession,
		pdfViewerEnabled,
		permissions,
		presentation,
		serviceWorker,
		storage,
		userActivation,
		wakeLock,
		webdriver,
		windowControlsOverlay,
	};

	const userAgentData = userAgent;

	// Firefox
	if (userAgentData.indexOf(BROWSER[1].name) > -1) {
		browserData.browser = BROWSER[1].info;
	}
	// Trident
	else if (userAgentData.indexOf(BROWSER[4].name) > -1) {
		browserData.browser = BROWSER[4].info;
	}
	// Opera
	else if (userAgentData.indexOf(BROWSER[3].name) > -1) {
		browserData.browser = BROWSER[3].info;
	}
	// Safari
	else if (userAgentData.indexOf(BROWSER[5].name) > -1) {
		// Chrome
		if (userAgentData.indexOf(BROWSER[0].name) > -1) {
			browserData.browser = BROWSER[0].info;
			// Edg
			if (userAgentData.indexOf(BROWSER[2].name) > -1) {
				browserData.browser = BROWSER[2].info;
			}
		} else {
			browserData.browser = BROWSER[5].info;
		}
	} else if (userAgentData.indexOf(BROWSER[6].name) > -1) {
		browserData.browser = BROWSER[6].info;
	} else {
		browserData.browser = 'unknown';
		browserData.isBrowser = false;
	}

	return browserData;
};
