/*
 * @Author: Do not edit
 * @Date: 2023-05-16 14:24:32
 * @LastEditTime: 2023-05-16 15:35:18
 * @LastEditors: 元辰
 * @PS: 获取电池状态信息（废弃❕）
 */
export const getBatteryView = () => {
	let battery = {
		charging: undefined,
		chargingTime: null,
		dischargingTime: null,
		level: null,
	};

	navigator.getBattery().then((batteryList) => {
		battery.charging = batteryList.charging;
		battery.chargingTime = batteryList.chargingTime;
		battery.dischargingTime = batteryList.dischargingTime;
		battery.level = batteryList.level;
	});

	return battery;
};
