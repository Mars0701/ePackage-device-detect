/*
 * @Author: Do not edit
 * @Date: 2023-05-16 14:52:26
 * @LastEditTime: 2023-05-16 15:34:01
 * @LastEditors: 元辰
 * @PS: 获取网络信息（实验性🧪）
 */
export const getConnectionView = () => {
	const { onLine, connection } = navigator;

	let webStatus = {
		onLine,
		downlink: {
			speed: `${connection.downlink}M/s`,
			value: connection.downlink,
			unit: `M/s`,
		},
		effectiveType: connection.effectiveType,
		onchange: connection.onchange,
		rtt: connection.rtt,
		saveData: connection.saveData,
	};

	return webStatus;
};
