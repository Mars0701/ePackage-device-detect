/*
 * @Author: 元辰
 * @Date: 2023-05-15 17:11:02
 * @LastEditTime: 2023-05-16 16:00:34
 * @LastEditors: 元辰
 * @PS: 获取设备信息
 */
export const getDeviceView = () => {
	const { maxTouchPoints, mediaDevices, userAgent } = navigator;

	let device = {
		maxTouchPoints,
		mediaDevices: {
			enumerateDevices: mediaDevices.enumerateDevices,
			getSupportedConstraints: mediaDevices.getSupportedConstraints,
			getDisplayMedia: mediaDevices.getDisplayMedia,
			getUserMedia: mediaDevices.getUserMedia,
		},
		type: 'unknown',
	};

	const userAgentData = userAgent;

	if (userAgentData.indexOf('iPad') > -1) {
		device.type = 'iPad';
	} else if (userAgentData.indexOf('Android') > -1 || userAgentData.indexOf('ios') > -1) {
		device.type = 'phone';
	} else {
		device.type = 'computer';
	}

	return device;
};
