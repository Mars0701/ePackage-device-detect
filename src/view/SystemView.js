/*
 * @Author: 元辰
 * @Date: 2023-05-15 17:11:02
 * @LastEditTime: 2023-05-16 16:03:59
 * @LastEditors: 元辰
 * @PS: 获取系统信息
 */
import { DEVICE } from '../utils/constant.js';

export const getSystemView = () => {
	const { userAgent, oscpu } = navigator;

	let systemData = {
		system: 'unknown',
		oscpu,
	};

	// Windows 10
	if (userAgent.indexOf(DEVICE[0].name) > -1) {
		systemData.system = DEVICE[0].info;
	}
	// Windows 8
	else if (userAgent.indexOf(DEVICE[1].name) > -1) {
		systemData.system = DEVICE[1].info;
	}
	// Windows 7
	else if (userAgent.indexOf(DEVICE[2].name) > -1) {
		systemData.system = DEVICE[2].info;
	}
	// Windows Vista
	else if (userAgent.indexOf(DEVICE[3].name) > -1) {
		systemData.system = DEVICE[3].info;
	}
	// Windows XP
	else if (userAgent.indexOf(DEVICE[4].name) > -1) {
		systemData.system = DEVICE[4].info;
	}
	// Windows 2000
	else if (userAgent.indexOf(DEVICE[5].name) > -1) {
		systemData.system = DEVICE[5].info;
	}
	// Mac/iOS
	else if (userAgent.indexOf(DEVICE[6].name) > -1) {
		systemData.system = DEVICE[6].info;
	}
	// UNIX
	else if (userAgent.indexOf(DEVICE[7].name) > -1) {
		systemData.system = DEVICE[7].info;
	}
	// Linux
	else if (userAgent.indexOf(DEVICE[8].name) > -1) {
		systemData.system = DEVICE[8].info;
	}
	// Android
	else if (userAgent.indexOf(DEVICE[9].name) > -1) {
		systemData.system = DEVICE[9].info;
	}
	// Ios
	else if (userAgent.indexOf(DEVICE[10].name) > -1) {
		systemData.system = DEVICE[10].info;
	}

	return systemData;
};
