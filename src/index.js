/*
 * @Author: 元辰
 * @Date: 2023-05-15 17:06:28
 * @LastEditTime: 2023-05-16 15:29:09
 * @LastEditors: 元辰
 */
import { getBrowserView } from './view/BrowserView.js';
import { getSystemView } from './view/SystemView.js';
import { getBatteryView } from './view/BatteryView.js';
import { getConnectionView } from './view/ConnectionView.js';
import { getDeviceView } from './view/DeviceView.js';

const device = {};

device.getBrowserView = getBrowserView;
device.getSystemView = getSystemView;
device.getBatteryView = getBatteryView;
device.getConnectionView = getConnectionView;
device.getDeviceView = getDeviceView;

export { getBrowserView, getSystemView, getBatteryView, getConnectionView, getDeviceView };

export default device;
