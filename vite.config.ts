import { defineConfig } from "vite";

export default defineConfig({
  server: {
    host: "0.0.0.0"
  },
  build: {
    lib: {
      entry: './src/index.js',
      name: 'epackage-device',
      fileName: (format) => `epackage-device.${format}.js`
    },
    outDir: 'dist'
  }
})
