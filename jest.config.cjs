/*
 * @Author: Do not edit
 * @Date: 2023-05-15 17:16:50
 * @LastEditTime: 2023-05-15 18:01:26
 * @LastEditors: 元辰
 */
module.exports = {
  clearMocks: true,
  collectCoverage: true,
  coverageDirectory: "coverage",
};
