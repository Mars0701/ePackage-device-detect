module.exports = {
  env: {
    browser: true,
    es2020: true,
    node: true
  },
  parser: "@babel/eslint-parser",
  parserOptions: {
    ecmaVersion: 12,
    sourceType: "module",
    allowImportExportEverywhere: true,
    requireConfigFile: false,
  },
  rules: {
    // **** Error 禁止规则 *************************************

    // 禁止与 -0 比较
    'no-compare-neg-zero': 'error',
    // 禁止使用 const 关键字声明的变量
    'no-const-assign': 'error',
    // 禁止使用 debugger
    'no-debugger': "error",
    // 防止switch case下沉
    'no-fallthrough': "error",
    // 禁止与NaN比较，使用 Number.isNaN() 或全局 isNaN() 函数
    'use-isnan': "error",
    // 禁用 if、for、while 和 do...while 测试条件中的模糊赋值运算符
    'no-cond-assign': ["error", "except-parens"],
    // 禁止 function 出现重名参数
    'no-dupe-args': "error",
    // 禁止对象字面量中出现同名参数
    'no-dupe-keys': "error",
    // 禁止 switch case 出现重复 case 值
    'no-duplicate-case': "error",
    // 禁止正则表达式中的空字符类
    'no-empty-character-class': "error",
    // 禁止在 catch 子句中重新分配异常
    'no-ex-assign': "error",
    // 限制了括号的使用，只在有必要的地方使用
    'no-extra-parens': ["error", "functions"],
    // 禁止对函数声明重新赋值
    'no-func-assign': "error",
    // 禁止对import的变量重新赋值
    'no-import-assign': "error",
    // 禁止稀疏数组
    'no-sparse-arrays': "error",
    // 禁止不必要的分号
    "no-extra-semi": "error",
    // 禁止出现空语句块,允许catch出现空语句
    "no-empty": ["error", { "allowEmptyCatch": true }],



    // **** Warn 警告规则 **************************************

    // 不允许出现 return、throw、continue 和 break 后的不可达代码
    'no-unreachable': "warn",
    // 不允许出现未使用的变量、函数和函数参数
    'no-unused-vars': [
      "warn",
      {
        "vars": "all",
        "args": "after-used",
        "varsIgnorePattern": "[iI]gnored"
      }
    ],
    // 不允许在 "if、for、while、do...while、三元表达式" 中使用常量表达式
    'no-constant-condition': "warn",
    // 不允许丢失精度的数字
    'no-loss-of-precision': "warn",
    // 不允许"直接"调用 Object.prototypes 的内置属性
    'no-prototype-builtins': "warn",



    // **** Off 停用规则 **************************************

  },
}
