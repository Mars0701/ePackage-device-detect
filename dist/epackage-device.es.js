const s = [
  { name: "Chrome", info: "Google Chrome or Chromium" },
  //0
  { name: "Firefox", info: "Mozilla Firefox" },
  //1
  { name: "Edg", info: "Microsoft Edge" },
  //2
  { name: "Opera", info: "Opera" },
  //3
  { name: "Trident", info: "Microsoft Internet Explorer" },
  //4
  { name: "Safari", info: "Apple Safari" },
  //5
  { name: "KHTML", info: "Konqueror" }
  //6
], n = [
  { name: "Windows NT 10.0", info: "Windows 10" },
  //0
  { name: "Windows NT 6.2", info: "Windows 8" },
  //1
  { name: "Windows NT 6.1", info: "Windows 7" },
  //2
  { name: "Windows NT 6.0", info: "Windows Vista" },
  //3
  { name: "Windows NT 5.1", info: "Windows XP" },
  //4
  { name: "Windows NT 5.0", info: "Windows 2000" },
  //5
  { name: "Mac", info: "Mac/iOS" },
  //6
  { name: "X11", info: "UNIX" },
  //7
  { name: "Linux", info: "Linux" },
  //8
  { name: "Android", info: "Android" },
  //9
  { name: "ios", info: "IOS" }
  //10
], v = () => {
  const { userAgent: e, language: i, cookieEnabled: o, buildID: r, mediaSession: f, pdfViewerEnabled: m, permissions: l, presentation: g, serviceWorker: c, storage: w, userActivation: u, wakeLock: x, webdriver: O, windowControlsOverlay: p } = navigator;
  let t = {
    browser: "unknown",
    isBrowser: !0,
    language: i,
    cookieEnabled: o,
    buildID: r,
    mediaSession: f,
    pdfViewerEnabled: m,
    permissions: l,
    presentation: g,
    serviceWorker: c,
    storage: w,
    userActivation: u,
    wakeLock: x,
    webdriver: O,
    windowControlsOverlay: p
  };
  const a = e;
  return a.indexOf(s[1].name) > -1 ? t.browser = s[1].info : a.indexOf(s[4].name) > -1 ? t.browser = s[4].info : a.indexOf(s[3].name) > -1 ? t.browser = s[3].info : a.indexOf(s[5].name) > -1 ? a.indexOf(s[0].name) > -1 ? (t.browser = s[0].info, a.indexOf(s[2].name) > -1 && (t.browser = s[2].info)) : t.browser = s[5].info : a.indexOf(s[6].name) > -1 ? t.browser = s[6].info : (t.browser = "unknown", t.isBrowser = !1), t;
}, y = () => {
  const { userAgent: e, oscpu: i } = navigator;
  let o = {
    system: "unknown",
    oscpu: i
  };
  return e.indexOf(n[0].name) > -1 ? o.system = n[0].info : e.indexOf(n[1].name) > -1 ? o.system = n[1].info : e.indexOf(n[2].name) > -1 ? o.system = n[2].info : e.indexOf(n[3].name) > -1 ? o.system = n[3].info : e.indexOf(n[4].name) > -1 ? o.system = n[4].info : e.indexOf(n[5].name) > -1 ? o.system = n[5].info : e.indexOf(n[6].name) > -1 ? o.system = n[6].info : e.indexOf(n[7].name) > -1 ? o.system = n[7].info : e.indexOf(n[8].name) > -1 ? o.system = n[8].info : e.indexOf(n[9].name) > -1 ? o.system = n[9].info : e.indexOf(n[10].name) > -1 && (o.system = n[10].info), o;
}, h = () => {
  let e = {
    charging: void 0,
    chargingTime: null,
    dischargingTime: null,
    level: null
  };
  return navigator.getBattery().then((i) => {
    e.charging = i.charging, e.chargingTime = i.chargingTime, e.dischargingTime = i.dischargingTime, e.level = i.level;
  }), e;
}, T = () => {
  const { onLine: e, connection: i } = navigator;
  return {
    onLine: e,
    downlink: {
      speed: `${i.downlink}M/s`,
      value: i.downlink,
      unit: "M/s"
    },
    effectiveType: i.effectiveType,
    onchange: i.onchange,
    rtt: i.rtt,
    saveData: i.saveData
  };
}, D = () => {
  const { maxTouchPoints: e, mediaDevices: i, userAgent: o } = navigator;
  let r = {
    maxTouchPoints: e,
    mediaDevices: {
      enumerateDevices: i.enumerateDevices,
      getSupportedConstraints: i.getSupportedConstraints,
      getDisplayMedia: i.getDisplayMedia,
      getUserMedia: i.getUserMedia
    },
    type: "unknown"
  };
  const f = o;
  return f.indexOf("iPad") > -1 ? r.type = "iPad" : f.indexOf("Android") > -1 || f.indexOf("ios") > -1 ? r.type = "phone" : r.type = "computer", r;
}, d = {};
d.getBrowserView = v;
d.getSystemView = y;
d.getBatteryView = h;
d.getConnectionView = T;
d.getDeviceView = D;
export {
  d as default,
  h as getBatteryView,
  v as getBrowserView,
  T as getConnectionView,
  D as getDeviceView,
  y as getSystemView
};
